<?php

namespace Database\Seeders;

use App\Models\author;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        author::create([
            'name' => 'Gabo'
        ]);
        author::create([
            'name' => ' PHILIP K.'
        ]);
    }
}
