<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/book', [App\Http\Controllers\BookController::class, 'index'])->name('book.index');
Route::get('/book/list', [App\Http\Controllers\BookController::class, 'list'])->name('book.list');
Route::get('/book/{book}', [App\Http\Controllers\BookController::class, 'show'])->name('book.show');
Route::post('/book/{book}', [App\Http\Controllers\BookController::class, 'reserved'])->name('book.reserved');
// Route::resource('user', [UserController::class]);
Route::post('/user/{book}',[UserController::class,'unreserved'])->name('user.unreserved');
