<?php

namespace App\Http\Controllers;

use App\Models\book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('book.index');
    }
    public function list()
    {
        $books = book::where('reserved', 0)->with('authors')->get(['id', 'title']);

        return datatables($books)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\book  $book
     * @return \Illuminate\Http\Response
     */
    public function show($book)
    {
        // dd($book);
        $book = book::where('id', $book)->with('authors')->first();

        // return datatables($books)->toJson();
        return response()->json($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\book  $book
     * @return \Illuminate\Http\Response
     */
    public function reserved(Request $request, book $book)
    {   
        $request->validate(['days' => 'required']);
        // dd($request  );
        // return response()->json('Hola');
        $book->days = $request->days;
        $book->user_id = auth()->user()->id;
        $book->reserved = true;
        $book->update();
        return response()->json(['res' => 'Reservado con Exito'],200);
        // return redirect()->route('home')->with('swal', ['title' => 'Exito!', 'message' => strtoupper('Libro Reservado'), 'status' => 'success', 'showDenyButton' => 'true']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(book $book)
    {
        //
    }
}
