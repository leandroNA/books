<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PharIo\Manifest\Author;

class book extends Model
{
    use HasFactory;


    public function authors(){
        return $this->belongsToMany('App\Models\Author');
    }
}
