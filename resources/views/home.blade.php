@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-column align-items-center text-center">
                        <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                        <div class="mt-3">
                            <h4>{{ auth()->user()->name }}</h4>
                            <p class="text-secondary mb-1">Libros Reservador: {{ count($books ?? 0)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-title text-center">
            <br>
            <span class="pt-4 text-primary">Mis Reservas</span>
        </div>
        <div class="card-body">
            <div class="d-flex flex-column align-items-center text-center">
                <table id="example" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Author</th>
                            <th>Dias</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($books as $book)
                        <tr>
                            <td>{{ $book->title }}</td>
                            <td>@foreach($book->authors as $author)
                                {{$author->name}} -
                                @endforeach
                            </td>
                            <td>
                                {{\Carbon\Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d', $book->days))}}
                            </td>
                            <td>
                                <form action="{{ route('user.unreserved', $book->id) }}" method="POST" class="btn m-0 p-0" onclick="eliminar(this)">
                                    @csrf

                                    <button type="submit" class="btn btn-sm btn-outline-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Dias</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            order: false,
            language: {
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
    });
</script>
@endsection