@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-12 pt-4">
        <div class="card profile-header p-4">
            <div class="body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="profile-image float-md-right">
                            <img src="https://cdn.pixabay.com/photo/2016/03/31/20/51/book-1296045_960_720.png" width="250px" hight="70px" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-12">
                        <h4 class="m-t-0 m-b-0">
                            <strong>{{ $book->title }}</strong>
                        </h4>
                        <span class="job_post">
                            <strong>Autor(s): </strong>
                            @foreach($book->authors as $author)
                            {{$author->name}} -
                            @endforeach
                        </span>
                        <br>
                        <p><strong>Descripcion:</strong> {{ $book->description }}</p>
                        <form method="post" action="{{ route('book.reserved', $book) }}" enctype="multipart/form-data">
                            @csrf
                            <!-- <input type="hidden" value="{{ $book->id }}"> -->
                            <div class="form-group col-4">
                                <label for="Street">Fecha de Reserva</label>
                                <input type="date" name="days" class="form-control">
                            </div>
                            <br>
                            <button class="btn btn-info btn-round">Reservar</button>
                        </form>

                        <!-- <button class="btn btn-primary btn-round btn-simple">Message</button> -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('js')

@endsection