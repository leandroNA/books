@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <br>
            <div class="card">
                <div class="card-title text-center">
                    <br>
                    <span class="pt-4 text-primary">Libros Disponibles</span>
                </div>
                <div class="card-body">
                    <div class="d-flex flex-column align-items-center text-center">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Titulo</th>
                                    <th>Author</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="exampleModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detalle de Libro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="col-12">
                        <div class="card profile-header p-4">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="profile-image float-md-right">
                                            <img src="https://cdn.pixabay.com/photo/2016/03/31/20/51/book-1296045_960_720.png" width="250px" hight="70px" alt="">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <h4 class="m-t-0 m-b-0">
                                            <strong></strong>
                                        </h4>
                                        <span class="job_post">
                                            <strong>Autor(s): </strong>
                                            <p id="titlemodal"></p>
                                        </span>
                                        <br>
                                        <p><strong>Descripcion:</strong>
                                        <p id="descriptionmodal"></p>
                                        </p>
                                        <form method="post" action="" id="formreserved" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" id="idmodal" value="">
                                            <div class="form-group col-5">
                                                <label for="Street">Fecha de Reserva</label>
                                                <input type="date" name="days" class="form-control" id="datemodal">
                                            </div>
                                            <br>
                                            <!-- <button class="btn btn-info btn-round">Reservar</button> -->
                                            <div class="modal-footer">
                                                <!-- <button type="button" class="btn btn-outline-info bt-sm" data-dismiss="modal">Cerrar</button> -->
                                                <button type="submit" class="btn btn-outline-success bt-sm">Reservar</button>
                                            </div>
                                        </form>
                                        <!-- <button class="btn btn-primary btn-round btn-simple">Message</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


@endsection

@section('js')
<script>
    $(document).ready(function() {
        let table = $('#example').DataTable({
            ajax: "{{ route('book.list')}}",
            columns: [{
                    data: 'title'
                },
                {
                    data: 'authors[0].name'
                },
                {
                    "render": function(data, type, row, meta) {
                        // console.log(row)
                        return '<a class="btn btn-outline-info bt-sm" href="http://127.0.0.1:8000/book/' + row.id + '" data-id="' + row.id + '" data-toggle="modal" data-target="#exampleModal" id="btnVer">Ver</a>';
                    }
                },
            ],
            order: false,
            language: {
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });

        $('body').on('click', "#btnVer", function(e) {
            e.preventDefault();
            console.log(e)
            let id = $(this).data("id")
            // axios.get('/book/'+id)
            $("#exampleModal").modal('show')
            axios.get(`/book/${id}`)
                .then(function(response) {
                    console.log(response.data.title)
                    document.querySelector("#idmodal").value = response.data.id
                    document.querySelector("#titlemodal").innerHTML = response.data.title
                    document.querySelector("#descriptionmodal").innerHTML = response.data.description
                    document.querySelector("#descriptionmodal").innerHTML = response.data.description

                }).catch(function(error) {
                    console.log(error)
                })

            $('#formreserved').on('submit', function(e) {
                e.preventDefault()
                let form = new FormData(document.querySelector('#formreserved'))
                console.log($('#idmodal').val())
                $.ajax({
                    url: "/book/" + $("#idmodal").val(),
                    type: "post",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: $(this).serialize()

                }).done(function(response) {
                    table.ajax.reload()
                    $('#exampleModal').modal('hide')

                    swal.fire({
                        title: 'Exito',
                        text: response.res,
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 2000
                    });
                }).catch(function(error) {
                    console.log(error.responseJSON.errors)
                })
            });

        })

        // table.ajax.url( 'http://127.0.0.1:8000/book/1' ).load();
    });
</script>
@endsection